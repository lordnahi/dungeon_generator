export class Compas {
    constructor(up = true, down = true, left = true, right = true) {
        this.up = up;
        this.down = down;
        this.left = left;
        this.right = right;
    }
}
//# sourceMappingURL=compas.js.map