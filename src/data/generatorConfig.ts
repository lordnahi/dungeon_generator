import { IGeneratorConfig } from "./types";

export const generatorConfig: IGeneratorConfig = {
  tileSize: 8
};
