import { IGeneratorSpec } from "./types";

export const generatorSpec: IGeneratorSpec = {
  mapWidth: 500,
  mapHeight: 500,
  numberOfMainRooms: 15,
  numberOfTreasureRooms: 2,
  numberOfMiniBosses: 3
};
