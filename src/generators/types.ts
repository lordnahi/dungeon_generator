export interface IGeneratorSpec {
  mapWidth: number;
  mapHeight: number;
  numberOfMainRooms: number;
  numberOfTreasureRooms: number;
  numberOfMiniBosses: number;
}
